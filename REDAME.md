# 初始化数据库
把db_init.json文件拷贝到cloud functions下面

# 配置 tabbar
```json
	"tabBar":{
		"color":"#666",
		"selectedColor":"#f07373",
		"backgroundColor":"#fff",
		"list":[
			{
				"pagePath":"pages/tabbar/index/index",
				"iconPath":"static/home.png",
				"selectedIconPath":"static/home-active.png",
				"text":"首页"
			},{
				"pagePath":"pages/tabbar/follow/follow",
				"iconPath":"static/follow.png",
				"selectedIconPath":"static/follow-active.png",
				"text":"关注"
			},{
				"pagePath":"pages/tabbar/my/my",
				"iconPath":"static/my.png",
				"selectedIconPath":"static/my-active.png",
				"text":"我的"
			}
		]
	}
```

# 引用组件

```js
	<!-- 自定义组件 -->
	<navbar></navbar>

	//可以引入后使用也可以不引入直接使用
	import navbar from '@/components/navbar/navbar.vue'
	components:{
		navbar
	},
```
	直接使用
	//easyCom cmponents/组件名/组件名.vue   局部引入
	
# 自定义字体图标
1. [插件市场地址] (https://ext.dcloud.net.cn/?cat1=1&cat2=11)
2. ```html <uni-icons type="search" size="16" color="#999"></uni-icons>```

# 连接云数据 创建云函数
1. 云函数中获取数据
```js
	'use strict';
	//获取数据库的引用
	const db = uniCloud.database()
	exports.main = async (event, context) => {
		//获取label表的数据
		let label = await db.collection('label').get()
		
		//返回数据给客户端
		return {
			code: 200,
			msg: '请求成功',
			data: label.data
		}
	};
```
2. 页面中调用
```js
	getLabel(){
		uniCloud.callFunction({
			name: 'get_label'
		}).then(res => {
			const {result} = res
			this.tabList = result.data
			console.log(this.tabList)
		})
	}
```
# 请求API封装
1. 批量导出文件
```js
	//批量导出文件
	const requireApi = require.context(
		//api 目录的相对路径
		'.',
		//是否查询子目录
		false,
		//查询文件的一个后缀
		/.js$/
	)
	let module = {}
	requireApi.keys().forEach((key, index) => {
		if(key=== './index.js') return
		Object.assign(module, requireApi(key))
	})

	export default module
```
2. 封装http请求及返回状态处理
```js
export default function $http(options){
	const {url, data} = options
	return new Promise((reslove, reject) => {
		uniCloud.callFunction({
			name: url,
			data
		}).then(res => {
			if(res.result.code === 200){
				reslove(res.result)
			}else{
				reject(res.result)
			}
		}).catch(err => {
			reject(err)
		})
	})	
}
```
3. list.js封装方法
```js
import $http from '../http.js';
export const get_label = (data) => {
	return $http({
		url: 'get_label',
		data
	})
}
```
4. main.js引入并挂载
```js
import api from './common/api'
Vue.prototype.$api = api
```
5. 组件调用
```js
getLabel(){
	this.$api.get_label({
		name: 'get_label'
	}).then(res => {
		const {data} = res
		this.tabList = data
	})
}
```

# 封装tab、navbar、list-scroll、list-card组件
list-card封装多种模式(基础模式、多图模式、大图模式，根据传入的mode来显示对应的模式)

# 文字溢出显示...
```css
	overflow: hidden;
	text-overflow: ellipsis;
	display: -webkit-box;
	-webkit-line-clamp: 2;
	-webkit-box-orient: vertical;
```
# list-swiper、list-swipwe-item的封装与index改变传值，实现联动
# 新建get_list云函数
```js
	const db = uniCloud.database()
	exports.main = async (event, context) => {
		const list = await db.collection('article')
		.field({
			//true-值返回这个字段，false-表示不返回
			content: false
		})
		.get()
		
		//返回数据给客户端
		return {
			code: 200,
			msg: '请求成功',
			data: list.data
		}
	};
```

# 实现tab点击显示对应内容，分类,实现懒加载
```js
	const {name} = event
	// 聚合： 更近细化的去处理数据： 求和、分组、指定哪些字段
	const list = await db.collection('article')
	.aggregate()
	.match({
		classify: name
	})
	.project({
		content: false
	})
	.end()
```

# 在tab第一个位置插入"全部"分类
```js
	getLabel(){
		this.$api.get_label({
			name: 'get_label'
		}).then(res => {
			const {data} = res
			data.unshift({
				name: '全部'
			})
			this.tabList = data
		})
	}
```

# 查看已浏览过的分类时会重复请求数据的优化
```js
	//当数据不存在或者长度是0的情况下才去请求数据
	if(!this.listCatchData[current] || this.listCatchData[current].length === 0){
		this.getList(current)
	}
```

# 实现加载更多提示功能,分页
1. [插件市场地址] (https://ext.dcloud.net.cn/?cat1=1&cat2=11) 搜索loadmore

# 标签页数据处理，编辑标签，保存标签页数据，使用自定义事件同步首页标签数据

# 详情页面展示，内容预加载，详情富文本渲染(uParse富文本渲染插件)
```js
//home-detail.vue中引入富文本组件
<view class="detail-content-html">
	<u-parse :content="formData.content" :noData="noData"></u-parse>
</view>
import uParse from '@/components/gaoyia-parse/parse.vue'
components:{
	uParse
},
data() {
	return {
		formData: {},
		noData: '<p style="text-align: center;color: #333">详情加载中...</p>'
	}
}
```
```js
//App.vue中引入富文本样式
<style>
	/*每个页面公共css */
	@import 'components/gaoyia-parse/parse.css';
</style>
```
# 评论内容实现
1. 引入插件uni-popup、uni-transition
2. 布局 release.vue
```html
<uni-popup ref="popup" type="bottom" :maskClick="false">
	<view class="popup-wrap">
		<view class="popup-header">
			<text class="popup-header-item" @click="close">取消</text>
			<text class="popup-header-item" @click="submit">发布</text>
		</view>
		<view class="popup-content">
			<textarea class="popup-textarea" v-model="commentsValue" value="" maxlength="200" fixed
				placeholder="请输入评论内容" />
			<view class="popup-count">
				{{commentsValue.length}}/200
			</view>
		</view>
	</view>
</uni-popup>
```
